package com.mosiler.bring.api.resultClass;

/**
 * enum으로 api 요청 결과에 대한 code, message를 정의합니다.
 * @author LUKAS
 *
 */
public enum CommonResponse {
	COM_0000("COM_0000", "성공"),
	COM_1000("COM_1000", "Null Pointer Exception"),
	COM_1001("COM_1001", "Nested SQL Exception"),
	USR_3006("USR_3006", "카카오로 가입한 이메일 입니다. 카카오로 로그인해주세요."),
	USR_3010("USR_3010", "이미 이메일로 가입된 회원 입니다. 이메일로 로그인 해주세요.\\n\\n이메일: {0}\\n닉네임: {1}"),
    FAIL("FAL_0000", "실패"),
	FAIL_2("FAL_0000", "스케줄러에서 실행 중인 drivingNo가 아닙니다.");
	
    String code;
    String msg;

    CommonResponse(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
