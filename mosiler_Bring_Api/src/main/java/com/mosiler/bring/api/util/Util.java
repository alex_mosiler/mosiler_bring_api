package com.mosiler.bring.api.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

/**
 * @author : stkim
 * @comment :
 */
public class Util {

	public Util() {
		
	}
    
	/**
	 * @return 
	 * @throws NoSuchAlgorithmException 
	 * @throws ParseException 
	 * @comment : 고객 휴대폰 번호 암호화 MD5
	 */
	public static String encPhoneNumber(String phoneNumber) throws NoSuchAlgorithmException {
	    StringBuffer sbuf = new StringBuffer();
	    
	    MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(phoneNumber.getBytes());
		
		byte[] msgStr = md.digest() ;
	     
	    for(int i = 0; i < msgStr.length; i++) {
	        String tmpEncTxt = Integer.toHexString((int)msgStr[i] & 0x00ff) ;          
	        sbuf.append(tmpEncTxt) ;
	    }

	    return sbuf.toString() ;
	}
	
	/**
	 * @return 
	 * @throws ParseException 
	 * @comment : 골프 시간 계산
	 * 예약 시간에서 +10시간 
	 */
	public static String calGolfTime(String time) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
		Calendar cal = Calendar.getInstance();
		cal.setTime(df.parse(time));
		cal.add(Calendar.HOUR_OF_DAY, 10);
		
		return df.format(cal.getTime());
	}
	
	/**
	 * @comment : Null 여부체크
	 * 문자열이 null이 아니면 true, 아니면 false
	 */
	public static boolean isNotNull(String s) {
		return s != null && !"".equals(s) && !"null".equals(s);
	}
    
	/**
	 * @comment : Null 여부체크
	 * 문자열이 null이면 true, 아니면 false
	 */
	public static boolean isNull(String s) {
		return s == null || "".equals(s);
	}
	
	/**
	 * @comment : 인증번호 가져오기 
	 */
	public static String getAuthNum() {
		return Util.getAuthNum(8);
	}

	public static String getAuthNum(int length) {
		String authNum = String.valueOf(Math.random());
		//System.out.println(authNum);
		return authNum.substring(authNum.length()-length, authNum.length());		
	}

	
	/**
	 * @comment : 고유키값 만들기 
	 */
	public static String getPrimaryKey() {
		Random rnd = new Random();
		
		long rndVal = Math.abs(rnd.nextLong());
		String curDate = DateUtil.getCurrentDateInfo("4");
		
		return curDate + String.valueOf(rndVal);
	}
	
	/**
	 * @comment : 날짜 
	 */
	public static String getDate(String date) {
		String result = "";
		
		if(isNull(date) || date.length() != 8) {
			return date;
		}
		
		result += date.substring(0, 4) + "년 ";
		result += date.substring(4, 6) + "월 ";
		result += date.substring(6, 8) + "일";
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 전체 치환
	 */
	public static String ReplaceAll(String s, String s1, String s2) {
		if(s == null) {
			return null;
		}
		
		if(s.indexOf(s1) < 0) {
			return s;
		}

		int i = s.length();
		int j = s1.length();
		StringBuffer stringbuffer = new StringBuffer();
		int k = 0;
		int l = 0;
		for(;k<i;k=l) {
			l = s.indexOf(s1, l);
			if(l < 0) {
                break;
			}
			stringbuffer.append(s.substring(k, l));
			stringbuffer.append(s2);
			l += j;
		}

		stringbuffer.append(s.substring(k));
		return stringbuffer.toString();
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : array 를 string로 변환
	 */
	public static String arrayToString(String as[]) {
		String s = new String();
		if(as != null) {
			for(int i=0;i<as.length;i++) {
				if(i != as.length - 1) {
					s = s + as[i] + ",";
				} else {
					s = s + as[i];
				}
			}
		}
		return s;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getAlertMsg(String s) {
		s = ReplaceAll(s, "\n", "\\n");
		s = ReplaceAll(s, "\"", "'");
		return s;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 파일 확장자 분리
	 */
	public static String getExt(String s) {
		if(s == null) {
			return "";
		}
		if(s.indexOf(".") != -1) {
			String s2 = s.substring(s.lastIndexOf("."));
			return s2;
		} else {
			return "";
		}
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 파라미터 map형태로 분리
	 */
	public static Hashtable getParameter(HttpServletRequest httpservletrequest) {
		Enumeration enumeration = httpservletrequest.getParameterNames();
		Hashtable hashtable = new Hashtable();
		while(enumeration.hasMoreElements()) {
			String s = (String)enumeration.nextElement();
			String s1 = httpservletrequest.getParameter(s);
			if(s1 != null && !s1.equals("")) {
				hashtable.put(s, s1);
			}
		}
		return hashtable;
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 파라미터 가져오기
	 */
	public static String getParameter(HttpServletRequest httpservletrequest, String s) {
		String s1 = httpservletrequest.getParameter(s);
		if(s1 == null) {
			s1 = "";
		}
		return s1;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 파라미터 map형태로 분리
	 */
	public static Hashtable getParameterValues(HttpServletRequest httpservletrequest) {
		Enumeration enumeration = httpservletrequest.getParameterNames();
		Hashtable hashtable = new Hashtable();
		while(enumeration.hasMoreElements()) {
			String s = (String)enumeration.nextElement();
			String as[] = httpservletrequest.getParameterValues(s);
			if(as != null) {
				hashtable.put(s, as);
			}
		}
		return hashtable;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : split하여 배열로
	 */
	public static String[] getSplit(String s, String s1) {
		int i = 0;
		String as[] = null;
		if(s == null) {
			return null;
		}
		if(s != null && !s.equals("")) {
			StringTokenizer stringtokenizer = new StringTokenizer(s, s1);
			as = new String[stringtokenizer.countTokens()];
			while(stringtokenizer.hasMoreTokens()) { 
				as[i++] = stringtokenizer.nextToken();
			}
		}
		return as;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 문자를 날짜형태로 변환
	 */
	public static boolean isDate(String s) {
		if(s == null || s.length() != 8) {
			return false;
		}
		
		try {
			int i = Integer.parseInt(s.substring(0, 4));
			int j = Integer.parseInt(s.substring(4, 6));
			int k = Integer.parseInt(s.substring(6, 8));
			if(i < 1900 || i > 9999) {
				return false;
			}
			if(j < 1 || j > 12) {
				return false;
			}
			return k >= 1 && k <= 31;
		} catch(Exception _ex) {
			return false;
		}
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : date 인지 확인
	 */
	public static boolean isDateTime(String s) {
		if(s == null || s.length() != 14) {
			return false;
		}
		return isDate(s.substring(0, 8)) && isTime(s.substring(8, 14));
	}
    
	/**
	 * @method : isImageFile
	 * @date :
	 * @author : stkim
	 * @comment : 확장자가 이미지파일인지 확인
	 */
	public static boolean isImageFile(String s) {
		String s1 = getExt(s);
		return s1.equals(".gif") || s1.equals(".jpg") || s1.equals(".png") || s1.equals(".bmp");
	}
    
	
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 시간형태인지 확인
	 */
	public static boolean isTime(String s) {
		if(s == null || s.length() != 6) {
			return false;
		}
		
		try {
			int i = Integer.parseInt(s.substring(0, 2));
			int j = Integer.parseInt(s.substring(2, 4));
			int k = Integer.parseInt(s.substring(4, 6));
			if(i < 0 || i > 23) {
				return false;
			}
			if(j < 0 || j > 59) {
				return false;
			}
			return k >= 0 && k <= 59;
		} catch(Exception _ex) {
			return false;
		}
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : decode
	 */
	public static String nullTo(String s, String s1) {
		if(isNotNull(s)) {
			return s;
		} else {
			return s1;
		}
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String[] stringToArray(String s, String s1) {
		if(isNull(s)) {
			return null;
		}
		StringTokenizer stringtokenizer = new StringTokenizer(s, s1);
		int i = stringtokenizer.countTokens();
		if(i <= 0) {
			return null;
		}
		String as[] = new String[i];
		for(int j=0;j<i && stringtokenizer.hasMoreTokens();j++) {
			as[j] = stringtokenizer.nextToken();
		}

		return as;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 유니코드를 ksc5601로 변환
	 */
	public static String uni2Ksc(String s) {
		if(s == null) {
			return null;
		}
		
		try {
			return new String(s.getBytes("8859_1"), "KSC5601");
		} catch(Exception _ex) {
			return null;
		}
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : ksc를 유니코드로
	 */
	public static String ksc2Uni(String s) {
		if(s == null) {
			return null;
		}
		
        try {
        	return new String(s.getBytes("KSC5601"), "8859_1");
        } catch(Exception _ex) {
        	return null;
        }
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : euckr을 유니코드로
	 */
	public static String euckrUni(String s) {
		if(s == null) {
			return null;
		}
		
		try {
			return new String(s.getBytes("euc-kr"), "ISO8859_1");
		} catch(Exception _ex) {
			return null;
		}
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static void copy(String sin, String sout) throws IOException {
		FileInputStream in = new FileInputStream (sin);
		FileOutputStream out = new FileOutputStream (sout);
		synchronized(in) {
			synchronized(out) {
				byte[] buffer = new byte[256];
				while(true) {
					int byteRead = in.read (buffer);
					if(byteRead == -1) {
						break;
					}
					out.write (buffer, 0, byteRead);
				}
			}
		}
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 3자리마다 콤마
	 */
	public static String getMoneyText(String t) {
		int num = 0;
		String out = "";
		
		if(isNotNull(t)) {
			try {
				DecimalFormat a = new DecimalFormat("###,###,###,##0");
				num = Integer.parseInt(t);
				out = a.format(num);
			} catch(Exception e) {
				
			}
		}
		
		return out;
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String alert(String mesg) {
		return "<script>alert('" + mesg + "');</script>";
	}
		
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String alert(String mesg, String url) {
		return "<script>alert('" + mesg + "'); document.location.href = '" + url + "';</script>";
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String alertPopup(String mesg) {
		return "<script>alert('" + mesg + "'); window.close();</script>";
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String alertPopup(String mesg, String url) {
		return "<script>alert('" + mesg + "'); window.opener.location.href = '" + url + "'; window.close();</script>";
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getRequestURL(HttpServletRequest request) {
		String url = request.getRequestURI();
		boolean first = true;
		
		String key = null;
		String value = null;
		for(Enumeration e = request.getParameterNames();e.hasMoreElements();) {
			key = (String)e.nextElement();
			value = request.getParameter(key);
			if(isNotNull(value)) {
				url += first ? "?" + key + "=" + value : "&" + key + "=" + value;
			}
		}
		return url;
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : url 파라미터 분리
	 */
	public static String getParameterUrl(HttpServletRequest request) {
		String url = request.getRequestURI();
		boolean first = true;
		
		String key = null;
		String value = null;
		for(Enumeration e = request.getParameterNames();e.hasMoreElements();) {
			key = (String)e.nextElement();
			value = request.getParameter(key);
			if(isNotNull(value) && key.trim().equals("PAGE") == false) {
				url += first ? "?" + key + "=" + value : "&" + key + "=" + value;
				first = false;
			}
		}
		return url;
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static double twoDownDecimal(double d) {
		d = (d * 100);
		d = (int)d;
		return d / 100.0;
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String DetailEscape(String value) {
		String temp = value;
		
		if(temp.indexOf("&dol;") > -1) {
			temp = ReplaceAll(temp, "&dol;", "$");
		}
		if(temp.indexOf("&lt;") > -1) {
			temp = ReplaceAll(temp, "&lt;", "<");
		}
		if(temp.indexOf("&gt;") > -1) {
			temp = ReplaceAll(temp, "&gt;", ">");
		}
		if(temp.indexOf("&quot;") > -1) {
			temp = ReplaceAll(temp, "&quot;", "\"");
		}
		if(temp.indexOf("&amp;") > -1) {
			temp = ReplaceAll(temp, "&amp;", "&");
		}
		if(temp.indexOf("&nbsp;") > -1) {
			temp = ReplaceAll(temp, "&nbsp;", " ");
		}
		
		return temp;
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String chgCommentString(String orgValue) {
		String temp = orgValue;
		
		if(temp.indexOf("'") > -1) {
			temp = ReplaceAll(temp, "'", "&#39");
		}
		if(temp.indexOf("\"") > -1) {
			temp = ReplaceAll(temp, "\"", "&quot;");
		}
		
		return temp;
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String CReturn(String value) {
		String temp = value;
		
		if (temp.indexOf("<BR>") > -1) {
			temp = ReplaceAll(temp, "<BR>", "\r\n");
		}
		return temp;
	}
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String outConv(String orgStr) {
		if(orgStr == null) {
			return "";
		}

		orgStr = ReplaceAll(orgStr, "\n", "<BR>");
		orgStr = ReplaceAll(orgStr, "\r", "");
		return orgStr;
	}
	
	

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getOmittedString( String str, int len, String omitSymbol ) {
		StringBuffer stringBuffer = new StringBuffer();
    	
		String dot = "...";
        
		int counterChar = 0;
		int targetChar = 0;
		
		boolean targetCharCount = true;
		boolean isTwoByteCharacter = false;
        
		if(Util.isNull(str)) {
			return "";
		}
        
		if(Util.isNotNull(omitSymbol)) {
			dot = omitSymbol;
		}
        
		char[] strArray = str.toCharArray();
    	
		for(int j=0;j<strArray.length;j++ ) {
			isTwoByteCharacter = strArray[j] >= 44032 && strArray[j] <= 55203;
    		
			if(isTwoByteCharacter) {
				counterChar += 2;
			} else {
				counterChar++;
			}
			
			if(counterChar >= len && targetCharCount) {
				targetChar = j;
				targetCharCount = false;
			}
		}
    	
    	if(targetChar > 0) {
			stringBuffer.append(str.substring(0 , targetChar))
						.append(dot);
		} else {
			stringBuffer.append(str);
		}

    	str = stringBuffer.toString();
        
		return str;
    }

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String[] splitString(String content, int splitSize) {
		Vector vt = new Vector(10, 10);
		String ctmp = content;
		int cntlen = 0;
		int csize = 3999;
		if(content != null) {
			ctmp = content;
			cntlen = content.getBytes().length;
		} else {
			ctmp = " ";
			cntlen = 1;
		}
		if(splitSize > 0) {
			csize = splitSize - 1;
		} else {
			csize = 3999;
		}
        
		while(cntlen > 0) {
			int bylen;
			int strlen = bylen = 0;
			if(cntlen > csize) {
				do {
					if(bylen >= csize) {
						break;
					}
					char c = ctmp.charAt(strlen);
					bylen++;
					strlen++;
					if(c > '\377') {
						bylen++;
					}
				} while(true);
			} else {
				strlen = ctmp.length();
				bylen = ctmp.getBytes().length;
			}
			cntlen -= bylen;
			String ct = ctmp.substring(0, strlen);
			ctmp = ctmp.substring(strlen);
			vt.add(ct);
		}
		String rtnArr[] = null;
		rtnArr = new String[vt.size()];
		vt.copyInto(rtnArr);
		return rtnArr;
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String changeString(String str) {
		String temp = "";
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i) == '\'') {
				temp = temp+"&#39;";
			} else if(str.charAt(i) == '"') {
				temp = temp+"&#34;";
			} else if(str.charAt(i) == '-') {
				temp = temp+"&#45;";
			} else if(str.charAt(i) == '<') {
				temp = temp+"&lt;";
			} else if(str.charAt(i) == '>') {
				temp = temp+"&gt;";
			} else if(str.charAt(i) == '%') {
				temp = temp+"&#37;";
			} else if(str.charAt(i) == '!') {
				temp = temp+"&#33;";
			} else {
				temp= temp+str.charAt(i);
			}
		}
		return temp;
    }

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getComma(String str) {
		return getComma(str, true);
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getComma(String str, boolean isTruncated) {
		if(str == null) {
			return "";
		}
		if(str.trim().equals("")) {
			return "";
		}
		if(str.trim().equals("&nbsp;")) {
			return "&nbsp;";
		}
		int pos = str.indexOf(".");
		if(pos != -1) {
			if(!isTruncated) {
				DecimalFormat commaFormat = new DecimalFormat("#,##0.00");
				return commaFormat.format(Float.parseFloat(str.trim()));
			} else {
				DecimalFormat commaFormat = new DecimalFormat("#,##0");
				return commaFormat.format(Long.parseLong(str.trim().substring(0, pos)));
			}
		} else {
			DecimalFormat commaFormat = new DecimalFormat("#,##0");
			return commaFormat.format(Long.parseLong(str.trim()));
		}
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getComma(Long lstr) {
		DecimalFormat commaFormat = new DecimalFormat("#,##0");
		if(lstr == null) {
			return "";
		} else {
			return commaFormat.format(lstr);
		}
    }

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getZeroBaseString(int num, int size) {
		return getZeroBaseString(String.valueOf(num), size);
	}

	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getZeroBaseString(String num, int size) {
		StringBuffer strBuf = new StringBuffer();
		if(num.length() >= size) {
			return num;
		}
		for(int index=0;index<size-num.length();index++) {
			strBuf.append("0");
		}

		return strBuf.toString() + num;
	}
    
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static int getByteSize(String str) {        
		int counterChar = 0;
		boolean isTwoByteCharacter = false;

		char[] strArray = str.toCharArray();

		for(int j=0;j<strArray.length;j++) {
			isTwoByteCharacter = strArray[j] >= 44032 && strArray[j] <= 55203;
    		
			if(isTwoByteCharacter) {
				counterChar += 2;
			} else {
				counterChar++;
			}
		}

		return counterChar;
    }
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 무료충전소 HMAC-MD5 암호화
	 */
	public static String getHMACMD5(String s, String keyString) {
		String sEncodedString = null;
		
		try {
			SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacMD5");
			Mac mac = Mac.getInstance("HmacMD5");
			mac.init(key);

			byte[] bytes = mac.doFinal(s.getBytes("ASCII"));

			StringBuffer hash = new StringBuffer();

			for(int i=0;i<bytes.length;i++) {
				String hex = Integer.toHexString(0xFF &  bytes[i]);
				if(hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			sEncodedString = hash.toString();
		} catch(UnsupportedEncodingException e) {
			
		} catch(InvalidKeyException e) {
			
		} catch(NoSuchAlgorithmException e) {
			
		}
		
		return sEncodedString;
    }
	
	/**
	 * @author : stkim
	 * @date :
	 * @comment : 
	 */
	public static String getGoogleShortUrl(String url) {
		String googUrl = "https://www.googleapis.com/urlshortener/v1/url?shortUrl=http://goo.gl/fbsS&key=AIzaSyCR7IJFdXMpWZ3FP17SuwnK4CeZ9gG7Rlk";
		String shortUrl = url;
		
		HttpURLConnection conn = null;
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		
		try {			
			conn = (HttpURLConnection)new URL(googUrl).openConnection();
			conn.setDefaultUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write("{\"longUrl\":\"" + url + "\"}");
			wr.flush();

			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;

			while((line = rd.readLine()) != null) {
				if(line.indexOf("id") > -1) {
					shortUrl = line.substring(8, line.length() - 2);
					break;
				}
			}
		} catch(Exception e) {
			
		} finally {
			if(wr != null) {
				try {
					wr.close();
				} catch(Exception e) {}
			}
			if(rd != null) {
				try {
					rd.close();
				} catch(Exception e) {}
			}
			
			if(conn != null) {
				conn.disconnect();
				conn = null;
			}
		}
		
		return shortUrl;
	}
	
	public static int rand(int num) {
		Random rand = new Random();
		return (rand.nextInt()>>>1) %num;
	}
	
	public static List<String> getSplitList(String s, String s1) {
		List<String> as = new ArrayList<String>();
		if(s == null) {
			return null;
		}
		if(s != null && !s.equals("")) {
			StringTokenizer stringtokenizer = new StringTokenizer(s, s1);
			while(stringtokenizer.hasMoreTokens()) { 
				as.add(stringtokenizer.nextToken().trim());
			}
		}
		return as;
	}
	
	
	/**
	 * String s가 full_size 보다 작으면 나머지 뒷 자리를 공백(blank)로 채워서 full_size String을 리턴.
	 * String s가 full_size 보다 크면 full_size 만큼 자른 String을 리턴.
	 */
	public static String fillSpace(String s, int full_size) {
		String result = "";
		if(s == null) return StringUtils.rightPad("", full_size);
		if(s.equals("")) return StringUtils.rightPad("", full_size);

		result = s;
		try {
			byte[] b = s.getBytes("EUC-KR"); // 한글은 2byte처리
			//byte[] b = s.getBytes("UTF-8"); // 한글은 2byte처리
			
			int size = b.length;
			byte[] filling = null;

			try {
				if (full_size < size) // 요구된 사이즈 보다 현재가 크면 substring
				{
					result = result.substring(0, full_size);
				} else if (full_size > size) // 부족한 사이즈 만큼 null 로 채워진 byte[]를 만든다.
				{
					filling = new byte[full_size - size];
					for (int i = 0; i < filling.length; i++) {
						filling[i] = ' ';
					}
					result += new String(filling);
				}
			} catch (Exception e) {
			}
			
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} 
		
		return result;
	}

	/**
	 * String s가 full_size 보다 작으면 앞 자리를 0으로 채워서 full_size String을 리턴. String s가
	 * full_size 보다 크면 full_size 만큼 자른 String을 리턴.
	 */
	public static String fillZero(String s, int full_size) {
		String zero = "0";
		String result = "";
		if (s != null)
			result = s;

		int size = s.getBytes().length; // 한글은 2byte처리

		try {
			if (full_size < size) {
				result = result.substring(0, full_size);
			} else if (full_size > size) {
				for (int i = size; i < full_size; i++) {
					result = zero + result;
				}
			}
		} catch (Exception e) {

		}
		return result;
	}

	public static String getEmailStar(String email) {
		String out = "";
		
		if(Util.isNotNull(email))
		{
			String [] emailAry = email.split("@");
			if(emailAry.length == 2) {
				if(emailAry[0].length() > 5) {
					out = emailAry[0].substring(0, emailAry[0].length()-3)+"***@"+ emailAry[1];
				} else if(emailAry[0].length() > 2) {
					out = emailAry[0].substring(0, emailAry[0].length()-2)+"**@"+ emailAry[1];
				} else {
					out = email;
				}
			} else {

				if(emailAry[0].length() > 5) {
					out = emailAry[0].substring(0, emailAry[0].length()-3)+"***";
				} else if(emailAry[0].length() > 2) {
					out = emailAry[0].substring(0, emailAry[0].length()-2)+"**";
				} else {
					out = email;
				}
			
			}
			
		}
		return out;
	}


	public static String fileTimeName() {
		Random random = new Random();
		int rVal = random.nextInt(99);

		String newName = DateUtil.getToday("yyyyMMdd_HHmmss_S") + rVal;
		return newName;
	}
	
	public static String getPlayTime(String play_time){
		String playTime = "00:00";
		
		try{
			int time = Integer.parseInt(play_time);
			int min = time / 60;
			int sec = time % 60;
			
			String sMin = String.valueOf(min);
			String sSec = String.valueOf(sec);
			
			if(min < 10) sMin = "0" + min;
			if(sec < 10) sSec = "0" + sec;
			
			playTime = sMin + ":" + sSec;
		} catch(Exception e){
			
		}
		
		return playTime;
		
	}
	
	public static String getRemoteIP(HttpServletRequest request){
        String ip = request.getHeader("X-FORWARDED-FOR"); 
        
        //proxy 환경일 경우
        if (ip == null || ip.length() == 0) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        //웹로직 서버일 경우
        if (ip == null || ip.length() == 0) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        
        if (ip == null || ip.length() == 0) {
			ip = request.getHeader("REMOTE_ADDR");
		}

        if (ip == null || ip.length() == 0) {
            ip = request.getRemoteAddr() ;
        }
        
        return ip;
   }
	
	public String getClientIP(HttpServletRequest request) {

		String ip = request.getHeader("HTTP_X_FORWARDED_FOR"); 
	    
		if (ip == null || ip.length() == 0) {
			ip = request.getHeader("REMOTE_ADDR");
		}

		if (ip == null || ip.length() == 0) {
			ip = request.getRemoteAddr() ;
		}
		return ip;
	}
	
	public static String getLocalServerIp() {
		try
		{
		    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
		    {
		        NetworkInterface intf = en.nextElement();
		        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
		        {
		            InetAddress inetAddress = enumIpAddr.nextElement();
		            if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress())
		            {
		            	return inetAddress.getHostAddress().toString();
		            }
		        }
		    }
		}
		catch (SocketException ex) {}
		return null;
	}
	
	public static boolean isMobile(HttpServletRequest request) {
		boolean isMobile = false;
		String userAgent = (String)request.getHeader("User-Agent");
		String[] mobileos = {"iPhone", "iPod", "Android", "BlackBerry", "Nokia", "samsung", "lgtelecom", "symbian", "sony", "SCH-", "SPH-"};
		if(userAgent!=null && !userAgent.equals("")) {
			for (String m : mobileos) {
				if(userAgent.indexOf(m)>-1) {
					isMobile = true;
					break;
				}
			}
		}
		
		return isMobile;
	}
	
//	public static void main(String []args) {
//		//System.out.println(DateUtil.getToday("yyyyMMddHHmm"));
//		//System.out.println(Util.fillZero("1", 2));
////		System.out.println(Util.getAuthNum(6));
////		System.out.println(Util.getAuthNum());
//		System.out.println(Util.getLocalServerIp());
//		
//		 InetAddress ip;
//	        String hostname;
//	        try {
//	            ip = InetAddress.getLocalHost();
//	            hostname = ip.getHostName();
//	            System.out.println("Your current IP address : " + ip);
//	            System.out.println("Your current Hostname : " + hostname);
//
//	        } catch (UnknownHostException e) {
//
//	            e.printStackTrace();
//	        }
//	}
	
}