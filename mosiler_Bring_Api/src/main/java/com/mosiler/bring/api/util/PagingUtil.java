package com.mosiler.bring.api.util;

import java.util.ArrayList;

public class PagingUtil {
	
	private int countPerPage;
	private int blockPage;
	private int totalCount;
	private int currentPageNo;
	
	public PagingUtil(int countPerPage, int blockPage, int totalCount, int currentPageNo){
		this.countPerPage = countPerPage;
		this.blockPage = blockPage;
		this.totalCount = totalCount;
		this.currentPageNo = currentPageNo;
	}

	public int getCountPerPage() {
		return countPerPage;
	}

	public int getBlockPage() {
		return blockPage;
	}

	public int getTotalCount() {
		return totalCount;
	}
	
	public int getCurrentPageNo() {
		return currentPageNo;
	}
	
	public int getTotalPages() {
		return (int) Math.ceil((double) totalCount / countPerPage);
	}
	
	public int getStartPageNum() {
		return totalCount == 0 ? 0 : ((currentPageNo - 1) * countPerPage) + 1;
	}
	
	public int getStartPageNoForBoard() {
		return getStartPageNum() - 1 <= 0 ? 0 : getStartPageNum() - 1;
	}
	
	public int getEndPageNum() {
		return (getStartPageNum() + countPerPage - 1) > totalCount ? totalCount : (getStartPageNum() + countPerPage - 1);
	}
	
	public int getPrePageNum() {
		return (currentPageNo - 1) <= 0 ? 1 : (currentPageNo - 1);
	}
	
	public int getNextPageNum() {
		return (currentPageNo + 1) > getTotalPages() ? getTotalPages() : (currentPageNo + 1);
	}
	
	public int getRealStartPageNum() {
		return (totalCount - ((currentPageNo - 1) * countPerPage));
	}
	
	public int getRealEndPageNum() {
		return getRealStartPageNum() - countPerPage + 1 < 0 ? 1 : getRealStartPageNum() - countPerPage + 1;
	}
	
	public int getPreviousBlock() {
		return getStartBlock()-1 <= 1 ? 1 : getStartBlock()-1;
	}

	public int getNextBlock() {
		return getEndBlock()+1 > getTotalPages() ? getTotalPages() : getEndBlock()+1;
	}

	public int getCurrentBlock() {
		return (int) Math.ceil((double) ((getCurrentPageNo() - 1) / blockPage)) + 1;
	}
	
	public int getStartBlock() {
		return ((getCurrentBlock() - 1) * blockPage) + 1;
	}
	
	public int getEndBlock() {
		return (getStartBlock() + blockPage - 1) > getTotalPages() ? getTotalPages() : (getStartBlock() + blockPage - 1);
	}
	
	public ArrayList<Integer> getPages() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i = getStartBlock(); i <= getEndBlock(); i++) {
			result.add(i);
		}
		return result;
	}
	
	
}
