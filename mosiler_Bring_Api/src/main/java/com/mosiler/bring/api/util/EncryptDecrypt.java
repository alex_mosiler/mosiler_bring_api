package com.mosiler.bring.api.util;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecrypt {
	
	public static String salt = "123456opqrstuvwxyzabcdefghijklmn";
	public static byte[] ivBytes = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	
	public static Key generateKey(String algorithm, byte[] keyData) {
		SecretKeySpec keySpec = new SecretKeySpec(keyData, algorithm);
		return keySpec;
	}

	/**
	 * @comment : 
	 */
	public static String encrypt(String src) throws Exception {
		if(src == null || src.equals("")) {
			return "";
		}
		
		Key key = generateKey("AES", salt.getBytes("UTF-8"));
		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
		byte[] plain = src.getBytes("UTF-8");
		byte[] encrypt = cipher.doFinal(plain);
		
		return toHexString(encrypt);
	}

	/**
	 * @comment :
	 */
	public static String decrypt(String hex) throws Exception {
		if(hex == null || hex.equals("")) {
			return "";
		}
		
		Key key = generateKey("AES", salt.getBytes("UTF-8"));
		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
		byte[] encrypt = toBytesFromHexString(hex);
		byte[] decrypt = cipher.doFinal(encrypt);
		
		return new String(decrypt, "UTF-8");
	}

	public static byte[] toBytesFromHexString(String digits) throws IllegalArgumentException, NumberFormatException {
		if(digits == null) {
			return null;
		}
		
		int length = digits.length();
		if(length % 2 == 1) {
			throw new IllegalArgumentException("For input string: \"" + digits + "\"");
		}
		
		length = length / 2;
		byte[] bytes = new byte[length];
		for(int i=0;i<length;i++) {
			int index = i * 2;
			bytes[i] = (byte) (Short.parseShort(digits.substring(index, index + 2), 16));
		}
		
		return bytes;
	}

	public static String toHexString(byte[] bytes) {
		if(bytes == null) {
			return null;
		}

		StringBuffer result = new StringBuffer();
		for(byte b : bytes) {
			result.append(Integer.toString((b & 0xF0) >> 4, 16));
			result.append(Integer.toString(b & 0x0F, 16));
		}
		
		return result.toString();
	}
}