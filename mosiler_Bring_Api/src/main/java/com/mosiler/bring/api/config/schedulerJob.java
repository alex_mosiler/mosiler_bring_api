package com.mosiler.bring.api.config;

import java.util.List;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.json.*;
import com.mosiler.bring.api.domain.bringVO;
import com.mosiler.bring.api.service.bringService;

@Component
public class schedulerJob extends ThreadPoolTaskScheduler {
	private static final long serialVersionUID = 1L; 
	
	private final Logger logger = LogManager.getLogger(schedulerJob.class);
	String className = "schedulerJob";
	
	@Resource(name="bringService")
    private bringService bringService;
	
	// 실시간 드라이버 위치 정보 전송
	public Runnable procSendData(int drivingNo)  {
		return () -> {
			try {
				// 데이터 전달 받을 브링쪽 url
//				String targetUrl = "";
				JSONObject jsonData = new JSONObject();
			
				// 위치 정보 Select
				List<bringVO> locationData = bringService.getLocationData(drivingNo);
			
				jsonData.put("reservationNo", locationData.get(0).getDrivingNo());
				jsonData.put("driverNo", locationData.get(0).getDriverNo());
				jsonData.put("locationLat", locationData.get(0).getLat());
				jsonData.put("locationLng", locationData.get(0).getLng());
				jsonData.put("locationAddress", locationData.get(0).getAddress());
				jsonData.put("locationDt", locationData.get(0).getLocationDt());
	
				try {
//					HttpUtil.httpCommunication(targetUrl, "POST", "application/json; charset=UTF-8", "application/json", jsonData);
					
//					if{} {
//						
//					} else {
//						bringService.testSchdulerEnd(String.valueOf(drivingNo));
//					}
				} catch (Exception e) {
					logger.debug("["+className+"] Error - Connection error");
				}
				
				
				// 확인용 출력
				System.out.println("ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ");
				System.out.println("예약 번호 		 	>>   " + locationData.get(0).getDrivingNo());
				System.out.println("드라이버 번호 		>>   " + locationData.get(0).getDriverNo());
				System.out.println("위도 좌표 		 	>>   " + locationData.get(0).getLat());
				System.out.println("경도 좌표 		 	>>   " + locationData.get(0).getLng());
				System.out.println("위치 주소 		 	>>   " + locationData.get(0).getAddress());
				System.out.println("저장된 시간 	 	>>   " + locationData.get(0).getLocationDt());
				System.out.println("ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// http통신
        };
	}
}
