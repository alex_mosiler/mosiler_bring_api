package com.mosiler.bring.api.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
	
	private String year;
	private String month;
	private String day;
	private String hour;
	private String min;
	private String sec;

	/**
	 * 현재날짜 가져오기. 기본포맷적용(yyyy.MM.dd)
	 * yyyyMMddHHmmss
	 * @return 현재일자 yyyy.MM.dd형식
	 */
	public static String getToday() {
		return getToday("yyyy.MM.dd");
	}
	
	public static String getTodayFormat(String fromat) {
		return getToday(fromat);
	}

	/**
	 * 입력받은 형식에 맞는 현재날짜(시간) 가져오기
	 * 
	 * @param fmt
	 *            날짜형식
	 * @return 현재일자
	 */
	public static String getToday(String fmt) {
		SimpleDateFormat sfmt = new SimpleDateFormat(fmt);
		return sfmt.format(new Date());
	}
	
	public static String getFormatDate(String pattern, String dataString) {
		if(pattern==null || pattern.equals("")) {
			 pattern = "yyyy-MM-dd";
		}
		String str = "";
		try {
			Date date = new SimpleDateFormat(pattern).parse(dataString);
			str = date.toGMTString();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	public static String getTimestampToDate(String pattern, String timestampStr){
	    long timestamp = Long.parseLong(timestampStr);
	    Date date = new java.util.Date(timestamp*1000L); 
	    SimpleDateFormat sdf = new java.text.SimpleDateFormat(pattern); //"yyyy-MM-dd HH:mm:ss"
	    sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+9")); 
	    String formattedDate = sdf.format(date);
	    return formattedDate;
	}
	
	public static long getStrDateToTimestamp(String pattern, String timestampStr) throws ParseException{
		
		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);//"yyyy-MM-dd hh:mm:ss.SSS"
	    Date parsedDate = dateFormat.parse(timestampStr);
	    //Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
	    //return timestamp.toString();
	    long nowts = parsedDate.getTime() / 1000L;
	    return nowts;
	}

	
//	public static void main(String[] args) {
//		
//		
////		long timeStampS = DateUtil.getStrDateToTimestamp(startPattern, startDay);
////		long timeStampE = DateUtil.getStrDateToTimestamp(endPattern, endDay);
//		
//		//DateUtil.getTimestampToDate("yyyy-MM-dd HH:00:00", uxTimeE)
//		
//		String uxTimeS = "1594693458";
//		String uxTimeE = "1594694058";
//		
//		long res = 0,res1 = 0;
//		String res2 = "";
//		try {
//			//res = DateUtil.getStrDateToTimestamp("yyyy-MM-dd HH:mm:ss", uxTimeS);
//			//res1 = DateUtil.getStrDateToTimestamp("yyyy-MM-dd HH:mm:ss", uxTimeE);
//			
//			//res2 = DateUtil.getTimestampToDate("yyyy-MM-dd HH:00:00", "1594694354");
//			res = DateUtil.getStrDateToTimestamp("yyyy-MM-dd HH:mm:ss", DateUtil.getTodayFormat("yyyy-MM-dd 00:00:00"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
////		String res = DateUtil.getTimestampToDate("yyyy-MM-dd HH:mm:ss", uxTimeS);
////		
////		long res1 = 0;
////		try {
////			res1 = DateUtil.getStrDateToTimestamp("yyyy-MM-01", res);
////		} catch (ParseException e) {
////			e.printStackTrace();
////		}
//		
//		System.out.println("res : "+res);
//		System.out.println("res1 : "+res1);
//		System.out.println("res2 : "+res2);
//		
//		//System.out.println(DateUtil.getToday("yyyy-MM-dd"));
//	}
	  
	
	/**
	 * 현재일자 + month(개월수)날짜를 yyyyMMdd format으로 return한다.
	 * 
	 * @param month
	 * @return String
	 */
	public static String addMonth(int month) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, month);
		return new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
	}

	public static String addMonth(int month, String fmt) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, month);
		return new SimpleDateFormat(fmt).format(cal.getTime());
	}
	
    public static String addMonth(String date, int month, String currentPattern) {
        // return value
        String newDate = null;
        if(Util.isNull(date)) return "";
        try {
            Calendar cal = Calendar.getInstance();
            
            SimpleDateFormat formatter = new SimpleDateFormat(currentPattern);
            cal.setTime(formatter.parse(date));
            cal.add(Calendar.MONTH, month);
            
            newDate = new SimpleDateFormat(currentPattern).format(cal.getTime());
            
        } catch (ParseException pe) {
            pe.printStackTrace();
        } //
        return newDate;
    }
    
    /**
     * 현재일자 + day(일수)날짜를 yyyyMMdd format으로 return한다.
     * 
     * @param day
     * @return String
     */
    public static String addDay(int day, String fmt) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, day);
        return new SimpleDateFormat(fmt).format(cal.getTime());
    }
    
    public static String addDay(int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, day);
        return new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
    }
    
    public static String addDay(String date, int day, String currentPattern) {
        // return value
        String newDate = null;
        if(Util.isNull(date)) return "";
        try {
            Calendar cal = Calendar.getInstance();
            
            SimpleDateFormat formatter = new SimpleDateFormat(currentPattern);
            cal.setTime(formatter.parse(date));
            cal.add(Calendar.DATE, day);

            newDate = new SimpleDateFormat(currentPattern).format(cal.getTime());
        } catch (ParseException pe) {
            pe.printStackTrace();
        } //
        return newDate;
    } //

    public static long diffOfDate(String begin, String end) {
		long diffDays = -1;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			Date beginDate = formatter.parse(begin);
			Date endDate = formatter.parse(end);
			long diff = endDate.getTime() - beginDate.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return diffDays;
    }
    
	/**
	 * @comment : 
	 */
	public DateUtil() {
		try {
			Calendar calendar = getCurTimeStamp();
			year = String.valueOf(calendar.get(Calendar.YEAR));
			month = String.valueOf((calendar.get(Calendar.MONTH) + 1));
			day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
			hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
			min = String.valueOf(calendar.get(Calendar.MINUTE));
			sec = String.valueOf(calendar.get(Calendar.SECOND));
		} catch(Exception e) {

		}
	}
    
	/**
	 * @comment : 
	 */
	public DateUtil(String s) {
		try {
			year = s.substring(0, 4);
			month = s.substring(4, 6);
			day = s.substring(6, 8);
			hour = s.substring(8, 10);
			min = s.substring(10, 12);
			sec = s.substring(12, 14);
		} catch(Exception e) {

		}
	}

	/**
	 * @comment : 
	 */
	public static Calendar getCurTimeStamp() {
		Locale lc = new Locale("Locale.KOREAN", "Locale.KOREA");
		TimeZone mySTZ = (TimeZone) TimeZone.getTimeZone("JST");
		Calendar calendar = Calendar.getInstance(mySTZ, lc);

		return calendar;
	}
 
	/**
	 * @comment : 
    	1 : YYYYMMDD
    	2 : YYYY년 MM월 DD일 
    	3 : YYYY.MM.DD
    	4 : YYYYMMDDHHMISS 
    	5 : YYYY/MM/DD
    	6 : YYYYMM
    	7 : YYYY/MM/DD HH:MI:SS
	 */
	public static String getCurrentDateInfo(String part) {
		String returnDate = "";

		Calendar calendar = getCurTimeStamp();
		int year = calendar.get(Calendar.YEAR);
		int month = (calendar.get(Calendar.MONTH) + 1);
		int day_of_month = calendar.get(Calendar.DAY_OF_MONTH);
		int hh = calendar.get(Calendar.HOUR_OF_DAY);
		int mi = calendar.get(Calendar.MINUTE);
		int ss = calendar.get(Calendar.SECOND);

		if(part.equals("1")) {
			returnDate += year;
			if(month < 10) {
				returnDate += "0";
			}
			returnDate += month;
			if(day_of_month < 10) {
				returnDate += "0";
			}
			returnDate += day_of_month;
		} else if(part.equals("2")) {
			returnDate += year + "년 ";
			returnDate += month + "월 ";
			returnDate += day_of_month + "일";
		} else if(part.equals("3")) {
			returnDate += year + ".";
			returnDate += month + ".";
			returnDate += day_of_month;
		} else if(part.equals("4")) {
			returnDate += year;
		
			if(month < 10) returnDate += "0";
			returnDate += month;
			
			if(day_of_month < 10) returnDate += "0";
			returnDate += day_of_month;
			
			if(hh < 10) returnDate += "0";
			returnDate += hh;
			
			if(mi < 10) returnDate += "0";
			returnDate += mi;
			
			if(ss < 10) returnDate += "0";
			returnDate += ss;
		} else if (part.equals("5")) {
			returnDate += year + "/";
			
			if(month < 10) returnDate += "0";
			returnDate += month + "/";
			
			if(day_of_month < 10) returnDate += "0";
			returnDate += day_of_month;
		} else if(part.equals("6")) {
			returnDate += year;
			if(month < 10) {
				returnDate += "0";
			}
			returnDate += month;
		} else if(part.equals("7")) {
			returnDate += year + "/";
			
			if(month < 10) returnDate += "0";
			returnDate += month + "/";
			
			if(day_of_month < 10) returnDate += "0";
			returnDate += day_of_month + " ";
			
			if(hh < 10) returnDate += "0";
			returnDate += hh + ":";
			
			if(mi < 10) returnDate += "0";
			returnDate += mi + ":";
			
			if(ss < 10) returnDate += "0";
			returnDate += ss;
		} else {}
		
		return returnDate;
	}
    
    /**
	 * @comment : 
    	1 : YYYYMMDD
    	2 : YYYY년 MM월 DD일 
    	3 : YYYY.MM.DD
    	4 : YYYYMMDDHHMISS 
    	5 : YYYY/MM/DD
    	6 : YYYYMM
	**/
	public static String getCurrentDateInfo(String part, String field, int amount) {
		String returnDate = "";

		Calendar calendar = getCurTimeStamp();
		if(field.equals("year")) {
			calendar.add(Calendar.YEAR, amount);
		} else if(field.equals("month")) {
			calendar.add(Calendar.MONTH, amount);
		} else if(field.equals("day")) {
			calendar.add(Calendar.DAY_OF_MONTH, amount);
		} else if(field.equals("hour")) {
			calendar.add(Calendar.HOUR_OF_DAY, amount);
		} else if(field.equals("minute")) {
			calendar.add(Calendar.MINUTE, amount);
		} else if(field.equals("second")) {
			calendar.add(Calendar.SECOND, amount);
		} else {

		}
    	
		int year = calendar.get(Calendar.YEAR);
		int month = (calendar.get(Calendar.MONTH) + 1);
		int day_of_month = calendar.get(Calendar.DAY_OF_MONTH);
		int hh = calendar.get(Calendar.HOUR_OF_DAY);
		int mi = calendar.get(Calendar.MINUTE);
		int ss = calendar.get(Calendar.SECOND);

		if(part.equals("1")) {
			returnDate += year;
			if(month < 10) {
				returnDate += "0";
			}
			returnDate += month;
			if(day_of_month < 10) {
				returnDate += "0";
			}
			returnDate += day_of_month;
		} else if(part.equals("6")) {
			returnDate += year;
			if(month < 10) {
				returnDate += "0";
			}
			returnDate += month;
		} else if(part.equals("2")) {
			returnDate += year + "년 ";
			returnDate += month + "월 ";
			returnDate += day_of_month + "일";
		} else if(part.equals("3")) {
			returnDate += year + ". ";
			returnDate += month + ". ";
			returnDate += day_of_month;
		} else if(part.equals("4")) {
			returnDate += year;
		
			if(month < 10) returnDate += "0";
			returnDate += month;
			
			if(day_of_month < 10) returnDate += "0";
			returnDate += day_of_month;
			
			if(hh < 10) returnDate += "0";
			returnDate += hh;
			
			if(mi < 10) returnDate += "0";
			returnDate += mi;
			
			if(ss < 10) returnDate += "0";
			returnDate += ss;
		} else if(part.equals("5")) {
			returnDate += year + "/";
			
			if(month < 10) returnDate += "0";
			returnDate += month + "/";
			
			if(day_of_month < 10) returnDate += "0";
			returnDate += day_of_month;
		} else if(part.equals("6")) {
			returnDate += year;
			if(month < 10) {
				returnDate += "0";
			}
			returnDate += month;
		} else {}
		
		return returnDate;
	}

	/**
	 * @comment : 
	 */
	public static String getDateTimeFormat(String s) {
		String dt = "";
		try {
			dt += s.substring (0, 4) + "년 ";
			dt += s.substring (4, 6) + "월 ";
			dt += s.substring (6, 8) + "일 ";
			int hour = Integer.parseInt(s.substring(8, 10));
			dt += hour > 13 ? " 오후 " : " 오전 ";
			hour -= hour > 13 ? 12 : 0;
			dt += (hour < 10 ? "0" + hour : Integer.toString(hour)) + "시 ";
			dt += s.substring(10, 12) + "분 ";
		} catch(Exception e) {
			return "";
		}

		return dt;
	}

	/**
	 * @comment : 
	 */
	public static String getDateTimeFormatEng(String s) {
		String dt = "";
		String[] month = {"X", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		try {
			dt += month[Integer.parseInt(s.substring (4, 6), 0)];
			dt += "-" + s.substring (6, 8);
			dt += "-" + s.substring (0, 4);
		} catch(Exception e) {
			return "";
		}

		return dt;
	}
	
	/**
	 * @comment : 
	 */
	public static String getDateTimeFormatEng(int month) {
		String[] arrMonth = {"X", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

		return arrMonth[month];
	}

	/**
	 * @comment : 
	 */
	public static String getSelectBoxYear(int sYear, String chkYear) {
		Calendar calendar = getCurTimeStamp();
		int pYear = calendar.get(Calendar.YEAR);
    	
		if(Util.isNull(chkYear)) {
			chkYear = String.valueOf(pYear);
		}
    	
		StringBuffer str = new StringBuffer();
		for(int i=pYear;i>=sYear;i--) {
    		if(String.valueOf(i).equals(chkYear)) {
    			str.append("<option value='"+i+"' selected>"+i+"</option>");
    		} else {
    			str.append("<option value='"+i+"'>"+i+"</option>");
    		}
    	}
		
		return str.toString();
	}
	
	/**
	 * @comment : 
	 */
	public static String getSelectBoxMonth(String chkMonth, String type) {
		Calendar calendar = getCurTimeStamp();
		int pMonth = (calendar.get(Calendar.MONTH) + 1);
		
		if(Util.isNull(chkMonth)) {
			chkMonth = String.valueOf(pMonth);
		}
		
		int maxMonth = 0;
		if(type.equals("A")) {
			maxMonth = pMonth;
		} else {
			maxMonth = 12;
		}
		
		StringBuffer str = new StringBuffer();
		String value = "";
		for(int i=maxMonth;i>=1;i--) {
			if(i < 10) {
				value = "0" + i;
			} else {
				value = String.valueOf(i);
			}
			if(String.valueOf(i).equals(chkMonth)) {
				str.append("<option value='"+value+"' selected>"+i+"</option>");
			} else {
				str.append("<option value='"+value+"'>"+i+"</option>");
			}
		}
		
		return str.toString();
	}
	
	/**
	 * @comment : 
	 */
	public static int getWeek() {
		return getCurTimeStamp().get(Calendar.DAY_OF_WEEK);
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getSec() {
		return sec;
	}

	public void setSec(String sec) {
		this.sec = sec;
	}
	
	/*
	public static void main(String [] args) {
		String start_date = DateUtil.addDay("20170124", 1, "yyyyMMdd");
		String end_date = DateUtil.addDay("20170124", 30, "yyyyMMdd");
		
		System.out.println(start_date);
		System.out.println(end_date);
		
		try {
			System.out.println(DateUtil.diffOfDate(start_date,end_date));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
}