package com.mosiler.bring.api.domain;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author alex
 * @Description 브링에서 요청 받은 데이터
 *	endDepartureTime : 브링에서 받는 데이터가 아닌 로직에서 계산 되어 들어가는 데이터
 * 	email : 브링에서 받는 데이터가 아닌 로직에서 계산 되어 들어가는 데이터
 */
@Builder // builder를 사용할수 있게 합니다.
@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
public class bringVO implements Serializable {
	private static final long serialVersionUID = -5566894170891530284L;

	// Json 파싱 상태값
	private boolean isStatus;						
	
	// 기존 존재 여부를 위해 
	private Integer CustomerNo;					
	private Integer DrivingNo;					
	
	// 브링 예약 데이터 기본 키값
	private String callId;								
	private String callType;						
	private String callStatus;						
	private String message;						
	
	// 예약자 정보
	private String reqUserId;						
	private String reqName;						
	private String reqPhoneNumber;			
	private String reqEmail;					
	
	// 탑승자 정보, 차량 정보
	private String reqPassenger;					
	private String reqPassengerPhoneNo;		
	private String reqCarName;				
	private String reqCarNumber;			
	private String reqOsType;						
	
	// 예약 시작 시간, 예약 종료 시간 (골프인 경우 +10시간)
	private String reqDepartureTime;			
	private String reqEndDepartureTime;		
	
	// 출발지 정보
	private String departureName;			
	private String departureLat;					
	private String departureLng;				

	// 도착지 정보
	private String arrivalName;				
	private String arrivalLat;					
	private String arrivalLng;					
	
	// 날짜
	private String updateAt;					
	private String createAt;
	
	// 위치 정보 데이터
	private int logNo;
	private int drivingNo;
	private int driverNo;
	private float lat;
	private float lng;
	String address;
	String locationDt;
	
	public boolean isStatus() {
		return isStatus;
	}
	public void setStatus(boolean isStatus) {
		this.isStatus = isStatus;
	}
	public Integer getCustomerNo() {
		return CustomerNo;
	}
	public void setCustomerNo(Integer customerNo) {
		CustomerNo = customerNo;
	}
	public Integer getDrivingNo() {
		return DrivingNo;
	}
	public void setDrivingNo(Integer drivingNo) {
		DrivingNo = drivingNo;
	}
	public String getCallId() {
		return callId;
	}
	public void setCallId(String callId) {
		this.callId = callId;
	}
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getCallStatus() {
		return callStatus;
	}
	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReqUserId() {
		return reqUserId;
	}
	public void setReqUserId(String reqUserId) {
		this.reqUserId = reqUserId;
	}
	public String getReqName() {
		return reqName;
	}
	public void setReqName(String reqName) {
		this.reqName = reqName;
	}
	public String getReqPhoneNumber() {
		return reqPhoneNumber;
	}
	public void setReqPhoneNumber(String reqPhoneNumber) {
		this.reqPhoneNumber = reqPhoneNumber;
	}
	public String getReqEmail() {
		return reqEmail;
	}
	public void setReqEmail(String reqEmail) {
		this.reqEmail = reqEmail;
	}
	public String getReqPassenger() {
		return reqPassenger;
	}
	public void setReqPassenger(String reqPassenger) {
		this.reqPassenger = reqPassenger;
	}
	public String getReqPassengerPhoneNo() {
		return reqPassengerPhoneNo;
	}
	public void setReqPassengerPhoneNo(String reqPassengerPhoneNo) {
		this.reqPassengerPhoneNo = reqPassengerPhoneNo;
	}
	public String getReqCarName() {
		return reqCarName;
	}
	public void setReqCarName(String reqCarName) {
		this.reqCarName = reqCarName;
	}
	public String getReqCarNumber() {
		return reqCarNumber;
	}
	public void setReqCarNumber(String reqCarNumber) {
		this.reqCarNumber = reqCarNumber;
	}
	public String getReqOsType() {
		return reqOsType;
	}
	public void setReqOsType(String reqOsType) {
		this.reqOsType = reqOsType;
	}
	public String getReqDepartureTime() {
		return reqDepartureTime;
	}
	public void setReqDepartureTime(String reqDepartureTime) {
		this.reqDepartureTime = reqDepartureTime;
	}
	public String getReqEndDepartureTime() {
		return reqEndDepartureTime;
	}
	public void setReqEndDepartureTime(String reqEndDepartureTime) {
		this.reqEndDepartureTime = reqEndDepartureTime;
	}
	public String getDepartureName() {
		return departureName;
	}
	public void setDepartureName(String departureName) {
		this.departureName = departureName;
	}
	public String getDepartureLat() {
		return departureLat;
	}
	public void setDepartureLat(String departureLat) {
		this.departureLat = departureLat;
	}
	public String getDepartureLng() {
		return departureLng;
	}
	public void setDepartureLng(String departureLng) {
		this.departureLng = departureLng;
	}
	public String getArrivalName() {
		return arrivalName;
	}
	public void setArrivalName(String arrivalName) {
		this.arrivalName = arrivalName;
	}
	public String getArrivalLat() {
		return arrivalLat;
	}
	public void setArrivalLat(String arrivalLat) {
		this.arrivalLat = arrivalLat;
	}
	public String getArrivalLng() {
		return arrivalLng;
	}
	public void setArrivalLng(String arrivalLng) {
		this.arrivalLng = arrivalLng;
	}
	public String getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(String updateAt) {
		this.updateAt = updateAt;
	}
	public String getCreateAt() {
		return createAt;
	}
	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getLogNo() {
		return logNo;
	}
	public void setLogNo(int logNo) {
		this.logNo = logNo;
	}
	public int getDriverNo() {
		return driverNo;
	}
	public void setDriverNo(int driverNo) {
		this.driverNo = driverNo;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLng() {
		return lng;
	}
	public void setLng(float lng) {
		this.lng = lng;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocationDt() {
		return locationDt;
	}
	public void setLocationDt(String lacationDt) {
		this.locationDt = lacationDt;
	}
	public void setDrivingNo(int drivingNo) {
		this.drivingNo = drivingNo;
	}
}
