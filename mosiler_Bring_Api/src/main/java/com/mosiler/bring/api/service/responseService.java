package com.mosiler.bring.api.service;

import org.springframework.stereotype.Service;
import com.mosiler.bring.api.resultClass.CommonResponse;
import com.mosiler.bring.api.resultClass.CommonResult;

@Service // 해당 Class가 Service임을 명시합니다.
public class responseService {

//    // 단일건 결과를 처리하는 메소드
//    public <T> SingleResult<T> getSingleResult(T data) {
//        SingleResult<T> result = new SingleResult<>();
//        result.setData(data);
//        setSuccessResult(result);
//        return result;
//    }
//    // 다중건 결과를 처리하는 메소드
//    public <T> ListResult<T> getListResult(List<T> list) {
//        ListResult<T> result = new ListResult<>();
//        result.setList(list);
//        setSuccessResult(result);
//        return result;
//    }
	
    // 성공 결과만 처리하는 메소드
    public CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }
    // 실패 결과만 처리하는 메소드
    public CommonResult getFailResult() {
        CommonResult result = new CommonResult();
        result.setSuccess(false);
        result.setCode(CommonResponse.FAIL.getCode());
        result.setMsg(CommonResponse.FAIL.getMsg());
        return result;
    }
    // 스케줄러 실패 결과만 처리하는 메소드
    public CommonResult getBatchFailResult() {
        CommonResult result = new CommonResult();
        result.setSuccess(false);
        result.setCode(CommonResponse.FAIL_2.getCode());
        result.setMsg(CommonResponse.FAIL_2.getMsg());
        return result;
    }
    // 결과 모델에 api 요청 성공 데이터를 세팅해주는 메소드
    private void setSuccessResult(CommonResult result) {
        result.setSuccess(true);
        result.setCode(CommonResponse.COM_0000.getCode());
        result.setMsg(CommonResponse.COM_0000.getMsg());
    }
}
