package com.mosiler.bring.api.util;

import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.*;
import com.mosiler.bring.api.domain.bringVO;

/**
 * @author alex
 *	@Description 브링에서 받은 Json 데이터 가공
 */
public class JsonDataToString {
	private final Logger logger = LogManager.getLogger(JsonDataToString.class);
	String className = "JsonDataToString";
	
	public bringVO bringData(HashMap<String, Object> param) {
		bringVO bringData = new bringVO();
		
		try {
			JSONObject jsonObject = new JSONObject(param);

			// 브링 예약 데이터 기본 키값
			bringData.setCallId(jsonObject.getString("callId"));
			bringData.setCallType(jsonObject.getString("callType").equals("GOLF")  ? "3" : "0");
			bringData.setCallStatus(jsonObject.getString("callStatus").equals("DISPATCHING")  ? "1" : "0");
			bringData.setMessage(jsonObject.getString("message"));

			// 예약자 정보
			bringData.setReqUserId(jsonObject.getString("reqUserId"));
			bringData.setReqName(jsonObject.getString("reqName"));
			bringData.setReqPhoneNumber(jsonObject.getString("reqPhoneNumber"));
			bringData.setReqEmail(bringData.getReqPhoneNumber()+"@mosiler.com");
			
			// 탑승자 정보, 차량 정보
			bringData.setReqPassenger(jsonObject.getString("reqPassenger"));
			bringData.setReqPassengerPhoneNo(jsonObject.getString("reqPassengerPhoneNo"));
			bringData.setReqCarName(jsonObject.getString("reqCarName"));
			bringData.setReqCarNumber(jsonObject.getString("reqCarNumber"));
			bringData.setReqOsType(jsonObject.getString("reqOsType"));
			
			// 예약 시작 시간, 예약 종료 시간 (골프인 경우 +10시간)
			bringData.setReqDepartureTime(jsonObject.getString("reqDepartureTime"));
			bringData.setReqEndDepartureTime(Util.calGolfTime(bringData.getReqDepartureTime()));
			
			// 출발지 정보
			bringData.setDepartureName(jsonObject.getString("departureName"));
			bringData.setDepartureLat(jsonObject.getJSONObject("departureLocation").getString("latitude"));
			bringData.setDepartureLng(jsonObject.getJSONObject("departureLocation").getString("longitude"));
			
			// 도착지 정보
			bringData.setArrivalName(jsonObject.getString("arrivalName"));
			bringData.setArrivalLat(jsonObject.getJSONObject("arrivalLocation").getString("latitude"));
			bringData.setArrivalLng(jsonObject.getJSONObject("arrivalLocation").getString("longitude"));
			
			// 날짜
			bringData.setUpdateAt(jsonObject.getString("updateAt"));
			bringData.setCreateAt(jsonObject.getString("createAt"));

			// 파싱 완료 시
	    	bringData.setStatus(true);	
		} catch (Exception e) {
			logger.info(" ========== JsonDataToString Error => " + this.className + " => bringData ==========");

			bringData = new bringVO();
			bringData.setStatus(false);
		}
		
		return bringData;	
	}
}
