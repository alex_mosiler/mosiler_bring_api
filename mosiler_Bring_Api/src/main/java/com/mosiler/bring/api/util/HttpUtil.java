package com.mosiler.bring.api.util;

import java.io.BufferedReader;


import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.JSONParser;
import org.json.*;

public class HttpUtil 
{
	private final static Logger logger = LogManager.getLogger(HttpUtil.class);
	
	// Http Restful API (common)
	public static Object httpCommunication(String targetUrl, String httpMethod, String contentType, String accept, JSONObject jsonData) throws Exception {
		JSONParser jsonParser = new JSONParser();
		Object jsonObj = new JSONObject();
		
		URL url = new URL(targetUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

//		connection.setConnectTimeout(10000);
//		connection.setReadTimeout(10000);
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", contentType);
		connection.setRequestProperty("Accept", accept);
		connection.setRequestMethod(httpMethod);                
		
		OutputStream os = connection.getOutputStream();
		os.write(jsonData.toString().getBytes("UTF-8"));
		os.flush();
		os.close();
		
		if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
		    String line = null;
		    
		    while((line = br.readLine()) != null) {
		    	jsonObj = jsonParser.parse(line);
		    }
		    
		    br.close();
		} 
		else {
			// 예외 상황 처리 필요
			logger.info(connection.getResponseCode());
		}
		
		connection.disconnect();
		
		return jsonObj;
	}

//	public static Object sendPut(String targetUrl, JSONObject jsonData) throws Exception {
//		JSONParser jsonParser = new JSONParser();
//		Object jsonObj = new JSONObject();
//		
//		URL url = new URL(targetUrl);
//		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//
//		connection.setDoOutput(true);
//		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//		connection.setRequestProperty("Accept", "application/json");
//		connection.setRequestMethod("PUT");                
//		
//		OutputStream os = connection.getOutputStream();
//		os.write(jsonData.toString().getBytes("UTF-8"));
//		os.flush();
//		os.close();
//		
//		if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
//			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
//		    String line = null;  
//		    
//		    while((line = br.readLine()) != null) {  
//		    	jsonObj = jsonParser.parse(line);
//		    }
//		    
//		    br.close();
//		}
//		
//		connection.disconnect();
//		
//		return jsonObj;
//	}
}
