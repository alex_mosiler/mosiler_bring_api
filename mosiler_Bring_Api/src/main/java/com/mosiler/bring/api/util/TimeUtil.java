package com.mosiler.bring.api.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TimeUtil {
	
	static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 */
	public static String getCurrentUnix_timestamp() {
		Date now = new Date();
        long nowts = now.getTime() / 1000L;
        
		/*
		 * System.out.println("nowts : "+nowts);
		 * 
		 * long ut1 = (nowts-86399);
		 * 
		 * //System.out.println("ut1 : "+ut1);
		 * 
		 * System.out.println("nowts-86399  : "+ut1);
		 * 
		 * Timestamp ts = new Timestamp(nowts); Date time1 = new Date(ts.getTime());
		 * 
		 * System.out.println("now : "+dateFormat.format(now));
		 * System.out.println("time5 : "+dateFormat.format(time1));
		 * //System.out.println("time5 : "+ts.getDate());
		 */
        
        //System.out.println("Current : "+nowts);
        
		return nowts+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampSecond0() {
		Date now = new Date();
		now.setSeconds(0);
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            초를 59으로 만듬
	 */
	public static String getCurrentUnix_timestampSecond59() {
		Date now = new Date();
		now.setSeconds(59);
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            1분후 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampMinutes1() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, +1);
        c.set(Calendar.SECOND, 0);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            3분전 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampMinutes3() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, -3);
        c.set(Calendar.SECOND, 0);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            5분전 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampMinutes5() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, -5);
        c.set(Calendar.SECOND, 0);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            15분전 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampMinutes15() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, -15);
        c.set(Calendar.SECOND, 0);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
        
//        Date time5 = new Date(ut3);
//		System.out.println("time5 : "+dateFormat.format(time5));
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            30분전 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampMinutes30() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, -30);
        c.set(Calendar.SECOND, 0);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            60분전 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampMinutes60() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR, -1);
        c.set(Calendar.SECOND, 0);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
		return ut3+"";
	}
	
	/**
	 * @comment : 현재시간  unix_timestamp
	 *            60분전 를 초를 0으로 만듬
	 */
	public static String getCurrentUnix_timestampDay1() {
		
        Date date = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        //c.set(Calendar.HOUR, -24);
        c.add(Calendar.HOUR, -24);
//        c.set(Calendar.AM_PM, 0);
//        c.set(Calendar.HOUR, 0);
//        c.set(Calendar.MINUTE, 0);
//        c.set(Calendar.SECOND, 0);

//        System.out.println(c.getTime());
        
        //c.add(Calendar.HOUR, -24);
        
        Date now = c.getTime();
        long ut3 = now.getTime() / 1000L;
        
        //System.out.println("day : "+ut3);
        
		return ut3+"";
	}
	
	public static String getTimestampToDate(String timestampStr, String format){
	    long timestamp = Long.parseLong(timestampStr);
	    Date date = new java.util.Date(timestamp*1000L); 
	    SimpleDateFormat sdf = new java.text.SimpleDateFormat(format);//yyyy-MM-dd HH:mm:ss
	    sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+9")); 
	    String formattedDate = sdf.format(date);
	    return formattedDate;
	}


	
	
//	public static void main(String[] args) {
//		
//		System.out.println("start_time : "+getCurrentUnix_timestampDay1());
//		System.out.println("end_time : "+getCurrentUnix_timestamp());
//		
//		
//		System.out.println(Integer.parseInt(getCurrentUnix_timestamp())-86399);
//		
////		getCurrentUnix_timestampDay1();
////		getCurrentUnix_timestamp();
//	}
	
	
	
}
