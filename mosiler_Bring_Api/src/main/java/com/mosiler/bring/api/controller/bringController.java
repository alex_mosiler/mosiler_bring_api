package com.mosiler.bring.api.controller;

import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mosiler.bring.api.resultClass.CommonResult;
import com.mosiler.bring.api.service.bringService;
import com.mosiler.bring.api.service.responseService;

@Controller
public class bringController {
	
	@Autowired
    private bringService bringService;
	
	@Autowired
	private responseService responseService;
	
    /**
     * 브링 예약 데이터 저장
     * @param param
     * @return
     * @throws Exception
     */
	@RequestMapping(value = "/bring/bringOrderInfo", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult insertBringOrder(@RequestBody HashMap<String, Object> param) throws Exception {
		String className = "insertBringOrder";
		CommonResult result = null;
		
		try {
			// 브링 예약 데이터 저장
			boolean isStatus = bringService.bringOrderInfo(param);
		
			// 결과 return
			if(isStatus) {
				result = responseService.getSuccessResult();
			} else {
				result = responseService.getFailResult();
			}
		} catch (Exception e) {
			result = responseService.getFailResult();
		}
	
		return result;
	}
	
	/**
	 * 테스트용 스케줄러 시작
	 * @param drivingNo
	 */	
	@RequestMapping(value="/startDrivingLogSchduler")
	@ResponseBody
    public CommonResult startDrivingLogSchduler(@RequestParam int drivingNo) {
    	// (해당 drivingNo에 대한) 스케줄러 Start
    	bringService.startDrivingLogSchduler(drivingNo);
    	
    	CommonResult result = responseService.getSuccessResult();
    	return result;
    }
    
	/**
	 * 테스트용 스케줄러 종료
	 * @param drivingNo
	 */
    @RequestMapping(value="/endDrivingLogSchduler")
    @ResponseBody
    public CommonResult endDrivingLogSchduler(@RequestParam int drivingNo) {
    	// (해당 drivingNo에 대한) 스케줄러 End
    	CommonResult result = bringService.endDrivingLogSchduler(String.valueOf(drivingNo));

    	return result;
    }
	
//    // post 테스트 코드
//    @ResponseBody
//    @RequestMapping(value = "/postTest", method = RequestMethod.POST)
//    public Object postTest() throws Exception {
//    	Object jsonData = bringService.postTest(); 
//    	return jsonData;
//    }
//    
//    // put 테스트 코드
//    @RequestMapping(value = "/putTest", method = RequestMethod.PUT)
//    public Object putTest() throws Exception {
//    	Object jsonData = service.putTest();
//    	return jsonData;
//    }
//    
//    // MS-SQL connection Test
//    @RequestMapping(value ="/dbTest", method = RequestMethod.GET)
//    public List<testVO> dbTest(HttpServletRequest request) {
//    	String id = request.getParameter("id");
//    	List<testVO> data = service.getData(id);
//    	
//    	return data;
//    }
} 