package com.mosiler.bring.api.config;

import java.util.concurrent.ScheduledFuture;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class schedulerSetting extends ThreadPoolTaskScheduler {
	 private static final long serialVersionUID = 1L;

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
        if(period <= 0) {
            return null;
        }
        
        ScheduledFuture<?> future = super.scheduleAtFixedRate(task, period);
        
        return future;
    }
}
