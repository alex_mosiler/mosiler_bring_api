package com.mosiler.bring.api.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.bring.api.domain.bringVO;

@Repository("com.mosiler.bring.api.mapper.bringMapper")
@Mapper
public interface bringMapper {
	int saveBringOrder(bringVO bringData);
	int insertCustomer(bringVO bringData);
	int insertReservation(bringVO bringData);
	int updateCustomer(bringVO bringData);
	int updateReservation(bringVO bringData);
	
	int saveKey(bringVO bringData);

	String selectBringKey(String callId);
	Integer selectDrivingKey(String bringKey);
	Integer selectCustomerKey(String bringKey);
	Integer selectCustomer(bringVO bringData);
	List<bringVO> selectLocationData(int drivingNo);
}
