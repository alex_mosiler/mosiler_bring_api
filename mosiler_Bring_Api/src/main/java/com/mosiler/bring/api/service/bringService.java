package com.mosiler.bring.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.mosiler.bring.api.config.schedulerJob;
import com.mosiler.bring.api.domain.bringVO;
import com.mosiler.bring.api.mapper.bringMapper;
import com.mosiler.bring.api.resultClass.CommonResult;
import com.mosiler.bring.api.util.JsonDataToString;
import com.mosiler.bring.api.util.Util;

@Service
public class bringService {
	private final Logger logger = LogManager.getLogger(bringService.class);
	String className = "bringService";
	
	// 스케줄러의 정보를 담을 Map
	private Map<String, ScheduledFuture<?>> scheduledTasks = new ConcurrentHashMap<>();
	
    @Autowired
    private TaskScheduler taskScheduler;
    
	@Autowired
	private schedulerJob schedulerJob;
	
	@Autowired
	private bringMapper bringMapper;

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private responseService responseService;
	
    /**
     * 브링 예약 데이터 저장
     * @param param
     * @return boolean
     * @throws Exception
     */
	public boolean bringOrderInfo(HashMap<String, Object> param) throws Exception {
		boolean isStatus = false;
		
		// JSON 파싱 객체
		JsonDataToString jsonData = new JsonDataToString();
		
		DefaultTransactionDefinition DTD = new DefaultTransactionDefinition();
		DTD.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus transactionStatus = transactionManager.getTransaction(DTD);
		
		try {
			// JSON 데이터 파싱
			bringVO bringData = jsonData.bringData(param);
			
			// 파싱, 세팅 과정 중에 에러 났을 경우
			if(!bringData.isStatus()) {
				return isStatus;
			}
			
			// 기본값 세팅
			bringData.setCustomerNo(0);
			bringData.setDrivingNo(0);
			
			// 브링 Call Id 조회 (기존 예약인지 아닌지)
			String bringKey = bringMapper.selectBringKey(bringData.getCallId());
			
			// 신규 예약 (bringKey가 없는 경우)
			if(bringKey == null || "".equals(bringKey)) {
				// 원본 데이터 저장
				int origin = bringMapper.saveBringOrder(bringData);
				
				// 고객 조회
				bringData.setCustomerNo(bringMapper.selectCustomer(bringData));
				
				// 고객 정보가 없을 때
				if(bringData.getCustomerNo() == null) {
					// 전화번호 암호화 (고객 ID)
					bringData.setReqUserId(Util.encPhoneNumber(bringData.getReqPhoneNumber()));
					
					// 고객 정보 Insert (return CustomerNo)
					bringMapper.insertCustomer(bringData);
				}
				
				// 예약 정보 Insert (return drivingNo)
				bringMapper.insertReservation(bringData);
				
				// 키값 insert (customerNo, drivingNo, bringCall_Id)
				if(origin == 1 && bringData.getCustomerNo() != 0 && bringData.getDrivingNo() != 0) {
					int check = bringMapper.saveKey(bringData);
					
					// DB Commit
					if(check == 1) {
						transactionManager.commit(transactionStatus);
						isStatus = true;		
					} else {
						logger.info(" ==========" + this.className + " => bringOrderInfo (insert Key Error) ==========");
						
						transactionManager.rollback(transactionStatus);
						
						return isStatus;
					}
				}
			}
			// 기존 예약 (bringKey가 있는 경우)
			else {
				// 기존 고객 
				bringData.setCustomerNo(bringMapper.selectCustomerKey(bringKey));
				
				// 기존 예약 
				bringData.setDrivingNo(bringMapper.selectDrivingKey(bringKey));
				
				// 원본 데이터 Insert
				int origin = bringMapper.saveBringOrder(bringData);
				
				// 고객 정보 Update
				int customer = bringMapper.updateCustomer(bringData);
				
				// 예약 정보 Update
				int reservation = bringMapper.updateReservation(bringData);
				
				if(origin == 1 && customer == 1 && reservation == 1) {
					// DB Commit
					transactionManager.commit(transactionStatus);
					isStatus = true;	
				}
				
				return isStatus;
			}
		} catch (Exception e) {
			logger.info(" ==========" + this.className + " => bringOrderInfo ==========");
			
			transactionManager.rollback(transactionStatus);
			
			return isStatus;
		}
		
		return isStatus;
	}
	
	/**
	 * 테스트용 스케줄러 시작
	 * @param drivingNo
	 */
	@Async
	public void startDrivingLogSchduler(int drivingNo) {
		System.out.println("Reservation Number("+drivingNo+") ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ The scheduler has Start.");
		
		// Job Start
		ScheduledFuture<?> task = taskScheduler.scheduleAtFixedRate(schedulerJob.procSendData(drivingNo), 1000);
		
		// Map에 해당 Task 저장
		scheduledTasks.put(String.valueOf(drivingNo), task);
	}

	/**
	 * 테스트용 스케줄러 종료
	 * @param drivingNo
	 * @return 
	 */
	public CommonResult endDrivingLogSchduler(String drivingNo) {
		CommonResult result = null;
		
		try {
			// Map에 저장된 Task 삭제
			scheduledTasks.get(drivingNo).cancel(true);
		
			System.out.println("Reservation Number("+drivingNo+") ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ The scheduler has Ended.");
			
			// 성공
			result = responseService.getSuccessResult();
		} catch (Exception e) {
			// 실패
			result = responseService.getBatchFailResult();
			return result;
		}
		
		return result;
	}
	 
	/**
	 * 드라이버 위치 정보
	 * @param drivingNo
	 * @return List<bringVO>
	 */
	public List<bringVO> getLocationData(int drivingNo) {
		return bringMapper.selectLocationData(drivingNo);
	}
    
//	// post 테스트 코드
//	@Override
//	public Object postTest() throws Exception {
//		Object jsonData = null;
//		
//		try {
//			String targetUrl = "https://api.bringmobility.com/api/v1/external/call";
//			
//			JSONObject json = new JSONObject();
//			json.put("callType", "GOLF");
//			
//			jsonData = HttpUtil.httpCommunication(targetUrl, "POST", "application/json; charset=UTF-8", "application/json", json);
//		} catch (Exception e) {
//		
//		}
//		
//		return jsonData;
//	}
//	
//	// put 테스트 코드
//	@Override
//	public Object putTest() throws Exception {
//		Object jsonData = null;
//		
//		try {
//			String targetUrl = "https://api.bringmobility.com/api/v1/external/call";
//
//			JSONObject json = new JSONObject();
//			json.put("callId", "5f430ef63334c54ddd5f1df6");
//			json.put("callStatus", "DISPATCHED");
//			json.put("driverName", "김기사");
//			json.put("driverPhone", "01000000000");
//			json.put("driverPicture", "https://randomuser.me/api/portraits/men/79.jpg");
//
//			jsonData = HttpUtil.httpCommunication(targetUrl, "PUT", "application/json; charset=UTF-8", "application/json", json);
//		} catch (Exception e) {
//		
//		}
//
//		return jsonData;
//	}
//
//    // MS-SQL connection Test
//	@Override
//	public List<testVO> getData(String id) {
//		return mapper.getData(id);
//	}
}
